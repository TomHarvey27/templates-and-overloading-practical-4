//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

#include "Array.hpp"

template <typename Type>
Type min (Type a, Type b)
{
    if(a <b)
        return a;
    else
        return b;
    
    
}

template <typename Type>
Type add (Type a, Type b)
{
   a = b + a;
    return a;
}


template <typename Type>
void print (Type a[], int b)
{
    int count = 0;
    
    while (count < b){
        
        std::cout << a[count] << std::endl;
        count++;
    }
    
 
}


template <typename Type>
double getAverge (Type a[], int b)
{
    int count = 0;
    int sum = 0;
    double result = 0;
    
    while (count < b){
        
        sum += a[count];
        count++;
    }
    
    result = a[count]/b;
    
}



int main (int argc, const char* argv[])
{
    
    
    
    Array<double> arrayF;
    
    double value[4] {5,6,7,8};
    double item;
    
    arrayF.add(value[0]);
    arrayF.add(value[1]);
    arrayF.add(value[2]);
    arrayF.add(value[3]);
    
    
    std::cout << arrayF.get(2); //Prints item at number 2 in array
    
    std::cout << arrayF.size(); // Prints the size of the array
    
    
    
    
    
    
    
    
    
    //int val1 = 4;
    int val1[4] = {4,5,6,7};
    int val2 = 4;
    
    //int result;
    double result;
    
    //result = min(val1, val2);
    //result = add(val1, val2);
    //print(val1, val2);
    
    
    //result = getAverge(val1, val2);
    
    
    //std::cout << result;
    
    return 0;
}

