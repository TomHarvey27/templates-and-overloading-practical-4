//
//  Array.hpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 04/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>
template <class Type>
class Array
{
public:
    
    Array()
    {
        floatPoint = nullptr;
        numItems = 0;
        
    }
    
    ~Array()
    {
        delete [] floatPoint;
        
    }
    void add(Type itemValue) // A new value is passed in
    {
        numItems ++; // for example that value could be 4th element in an array, but there are currently only 3
        
        Type* temp = nullptr;      // a tempoary array is created with extra space for the new value
        temp = new Type[numItems];
        
        for (int count= 0 ; count < numItems -1 ; count++) //each value from the current array is coppied into the temporary
        {
            temp[count] = floatPoint[count]; //The temp now has all the same as the previous
        }
        
        temp[numItems-1] = itemValue; // now it adds on the last one to the temp list
        floatPoint = temp; //it then copies it back to the floating point arrayx

    }
    
    Type get (int index)
    {
        float item = 0;
        
        if (index == 0)
        {
            
            return 0;
        }
        
        item = floatPoint[index];
        
        return item;
    }
    
    Type size()
    {
        int numOfArrays = numItems;
        
        return numOfArrays;
    }
    
private:
    
    Type *floatPoint;
    int numItems;
    
};


#endif /* Array_hpp */
