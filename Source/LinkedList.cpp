//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"



LinkedList::LinkedList()
{
    head = nullptr;
    numOfItems = 0;
    
}
LinkedList::~LinkedList()
{
    
    Node* deletePointer = head;  // Points to what head is pointing at
    
    
    while (deletePointer -> next != nullptr) // This will loop until at the end
    {
     
        head = head-> next;     // Equals the next item in the array
        delete deletePointer;   // This equals the head and is deleted
        deletePointer = head;   // the delete pointer now equals the heads current value
        
    }
    
    
    
    
}
void LinkedList::add (float itemValue)
{
    
    Node* newnode = new Node; // create a new node
    
    
    newnode -> value = itemValue;   //this nodes value is equal to the arugment
    newnode -> next = nullptr;      //This will be the last node and is equal to 0(the end)
    
    
    if (head == nullptr) // If the head is the only thing in the list(as in this is the first time)
    {
        
        head = newnode; // Head points to new node
    }
    
    else
    {
        Node* temp = head;          //This tempoary pointer, points to what 'head' is pointing at
        
        while (temp -> next != nullptr){    //If temps next value is 0, that means its at the end
            
            temp = temp -> next; // The first time round, the head equals the next value
                                // So in the loop, the current value(temp) is assigned to its next(which is a and adress)
        }                           // It gets to the end and pointer points to the last node in the list.
        
        temp -> next = newnode; // This assigns the temps next function to point at the new node.
        
    }
    
}
float LinkedList::get (int index)
{
    float item = 0;
    Node* temp = head;
    
  
    for (int counter = 0; counter < item; counter++)
    {
        temp = temp -> next; //temp now points to the next adress, until it equals 0
        
    }
    
    
    
    return item;
    
}
int LinkedList::size()
{
    
    Node* temp = head;  // Points to what head is pointing at
    int count= 0;
    
    while (temp -> next == nullptr)
    
    {
        temp = temp -> next; //temp now points to the next adress, until it equals 0
        count ++;   // This is for each node so determine the size
    }
    
    
    
    
    return count;
    
}